import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuNavComponent } from './menu-nav/menu-nav.component';
import { FooterComponent } from './footer/footer.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatListItem,MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule,MatAccordion,MatExpansionModule, MatInputModule} from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import {MatPaginator, MatTableDataSource} from '@angular/material';


import { InicioComponent } from './component/inicio/inicio.component';
import { HistoriaComponent } from './component/historia/historia.component';
import { TransparenciaComponent } from './component/transparencia/transparencia.component';
import { UbicacionComponent } from './component/ubicacion/ubicacion.component';
import { TablaContratoOBSComponent } from './component/transparencia/tabla-contrato-obs/tabla-contrato-obs.component';
import { ListaLeyGContabilidadComponent } from './component/transparencia/lista-ley-gcontabilidad/lista-ley-gcontabilidad.component';
import { TablaAvancegestion2018Component } from './component/transparencia/tabla-avancegestion2018/tabla-avancegestion2018.component';
import { TablaEstadosfinaciero2018Component } from './component/transparencia/tabla-estadosfinaciero2018/tabla-estadosfinaciero2018.component';
import { TablaLeydiciplinafinanciera2018Component } from './component/transparencia/tabla-leydiciplinafinanciera2018/tabla-leydiciplinafinanciera2018.component';
import { TablaAvancegestion2017Component } from './component/transparencia/tabla-avancegestion2017/tabla-avancegestion2017.component';
import { TablaReporte2017Component } from './component/transparencia/tabla-reporte2017/tabla-reporte2017.component';
import { TablaLeydiciplinariafinanciera2017Component } from './component/transparencia/tabla-leydiciplinariafinanciera2017/tabla-leydiciplinariafinanciera2017.component';
import { TablaEstadosfinancieros2017Component } from './component/transparencia/tabla-estadosfinancieros2017/tabla-estadosfinancieros2017.component';
import { GaleriaComponent } from './component/galeria/galeria.component';
import { Anexos2018Component } from './component/transparencia/anexos2018/anexos2018.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    MenuNavComponent,
    InicioComponent,
    HistoriaComponent,
    TransparenciaComponent,
    GaleriaComponent,
    UbicacionComponent,
    TablaContratoOBSComponent,
    ListaLeyGContabilidadComponent,
    TablaAvancegestion2018Component,
    TablaEstadosfinaciero2018Component,
    TablaLeydiciplinafinanciera2018Component,
    TablaAvancegestion2017Component,
    TablaReporte2017Component,
    TablaLeydiciplinariafinanciera2017Component,
    TablaEstadosfinancieros2017Component,
    Anexos2018Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatTabsModule,
    MatTableModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule
   ],
exports: [
    MatTableModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
