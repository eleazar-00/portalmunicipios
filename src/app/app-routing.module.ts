import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './component/inicio/inicio.component';
import { TransparenciaComponent } from './component/transparencia/transparencia.component';
import { HistoriaComponent } from './component/historia/historia.component';
import { UbicacionComponent } from './component/ubicacion/ubicacion.component';
import { GaleriaComponent } from './component/galeria/galeria.component';


const routes: Routes = [
  {path: '', redirectTo: '/inicio', pathMatch: 'full'},
  {path: 'inicio', component: InicioComponent},
  {path: 'historia', component: HistoriaComponent},
  {path: 'transparencia', component: TransparenciaComponent},
  {path:'galeria', component:GaleriaComponent},
  {path: 'ubicacion', component: UbicacionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
