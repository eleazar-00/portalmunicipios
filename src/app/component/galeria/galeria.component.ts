import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.css']
})
export class GaleriaComponent implements OnInit {
  panelOpenState = false;
  imagenes:String []=['assets/galeria/FAENA DEL PUEBLO-JORNADA/20180605_123906.jpg',
                      'assets/galeria/FAENA DEL PUEBLO-JORNADA/20180605_123910.jpg',
                      'assets/galeria/FAENA DEL PUEBLO-JORNADA/20180605_160937.jpg',
                      'assets/galeria/FAENA DEL PUEBLO-JORNADA/20180605_161026.jpg',
                      'assets/galeria/FAENA DEL PUEBLO-JORNADA/20180605_161030.jpg',
                      'assets/galeria/FAENA DEL PUEBLO-JORNADA/20180605_163535.jpg']
  imgDeportes:String []=['assets/galeria/FOMENTACIÓN DE DEPORTE/IMG_0471.JPG',
                         'assets/galeria/FOMENTACIÓN DE DEPORTE/IMG_0598.JPG',
                         'assets/galeria/FOMENTACIÓN DE DEPORTE/IMG_0599.JPG']
  imgUniformesPolis:String []=['assets/galeria/FOTO DE LA ENTREGA UNIFORME A LOS POLICIAS/20180623_111610.jpg',
                              'assets/galeria/FOTO DE LA ENTREGA UNIFORME A LOS POLICIAS/20180623_112836.jpg',
                              'assets/galeria/FOTO DE LA ENTREGA UNIFORME A LOS POLICIAS/IMG_0683.JPG',
                              'assets/galeria/FOTO DE LA ENTREGA UNIFORME A LOS POLICIAS/IMG_0691.JPG']
  imgMadres:String []=['assets/galeria/FOTOS DEL DIA DE LA MADRE/20180510_103945.jpg',
                              'assets/galeria/FOTOS DEL DIA DE LA MADRE/20180510_114141.jpg',
                              'assets/galeria/FOTOS DEL DIA DE LA MADRE/20180510_142821.jpg']
  imgNino:String []=['assets/galeria/FOTOS DEL DIA DEL NIÑO/20180430_135200.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL NIÑO/20180430_153010.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL NIÑO/20180430_153022.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL NIÑO/20180430_154133.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL NIÑO/20180430_154345.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL NIÑO/20180430_154358.jpg']
  imgPadre:String []=['assets/galeria/FOTOS DEL DIA DEL PADRE/20180617_151012.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL PADRE/20180617_151018.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL PADRE/20180617_151155.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL PADRE/20180617_213903.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL PADRE/20180617_225144.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL PADRE/20180617_225154.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL PADRE/20180617_225209.jpg',
                              'assets/galeria/FOTOS DEL DIA DEL PADRE/20180617_225222.jpg']
imgGAPrimaria:String []=['assets/galeria/GESTION DE DOS AULAS DE PRIMARIA/20180406_134311.jpg',
                              'assets/galeria/GESTION DE DOS AULAS DE PRIMARIA/IMG-20181220-WA0009.jpg']
imgAudiencias:String []=['assets/galeria/LAS AUDIENCIAS/20170313_195727.jpg',
                              'assets/galeria/LAS AUDIENCIAS/20170321_161653.jpg',
                              'assets/galeria/LAS AUDIENCIAS/20180323_130015.jpg',
                              'assets/galeria/LAS AUDIENCIAS/20180326_105654.jpg',
                              'assets/galeria/LAS AUDIENCIAS/20180801_120634.jpg',
                              'assets/galeria/LAS AUDIENCIAS/20180801_120638.jpg']
  imgAsamble:String []=['assets/galeria/SAMBLEA/IMG_20170318_104932.jpg' ]
  constructor() { }

  ngOnInit() {
  }

}
