import { Component, OnInit } from '@angular/core';

export interface lista {
  nombre: string;
  archivo: string;
}

@Component({
  selector: 'app-lista-ley-gcontabilidad',
  templateUrl: './lista-ley-gcontabilidad.component.html',
  styleUrls: ['./lista-ley-gcontabilidad.component.css']
})
export class ListaLeyGContabilidadComponent implements OnInit {

  listas: lista[] = [
    {archivo: 'assets/pdfs/2017/Iniciativa de la Ley de Ingresos.pdf', nombre: '1. Iniciativa de la Ley de Ingresos'},
    {archivo: 'assets/pdfs/2017/Proyecto de Presupuesto de Egresos Armonizado.pdf', nombre: '2. Proyecto del Presupuesto de Egresos'},
    {archivo: 'assets/pdfs/2017/Ley de Ingresos y Presupuesto de Egresos Ciudadano.pdf', nombre: '3. Ley de Ingresos y Presupuesto de egresos Ciudadano'},
    {archivo: 'assets/pdfs/2017/calendario mensual de ingresos.pdf', nombre: '4. Calendario Mensual de Ingresos'},
    {archivo: 'assets/pdfs/2017/calendario mensual de egresos.pdf', nombre: '5. Calendario Mensual de Egresos'},
    {archivo: '#', nombre: '6. Cuenta Pública'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
