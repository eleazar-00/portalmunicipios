import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaLeyGContabilidadComponent } from './lista-ley-gcontabilidad.component';

describe('ListaLeyGContabilidadComponent', () => {
  let component: ListaLeyGContabilidadComponent;
  let fixture: ComponentFixture<ListaLeyGContabilidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaLeyGContabilidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaLeyGContabilidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
