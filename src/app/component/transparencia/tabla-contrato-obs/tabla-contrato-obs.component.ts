import { Component, OnInit } from '@angular/core';

export interface ContratosOBS {
  obras: string;
  contratos: string;
  facturas: string;

}

const ELEMENT_DATA: ContratosOBS[] = [
  {obras: "CONTRATO AGUA POTABLE 1 ETAPA SAN JUAN ZAPOTITLAN", contratos: "assets/pdfs/obras/contratos/1 CONTRATO AGUA POTABLE 1 ETAPA SAN JUAN ZAPOTITLAN.pdf", facturas: ""},
  {obras: 'CONTRATO CALLE ACUEDUCTO 1 ETAPA SAN PEDRO SOCHIAPAM', contratos: 'assets/pdfs/obras/contratos/3 CONTRATO CALLE ACUEDUCTO 1 ETAPA SAN PEDRO SOCHIAPAM.pdf', facturas:  ''},
  {obras: 'CONTRATO CALLE 1 ETAPA SAN JOSE RETUMBADERO', contratos: 'assets/pdfs/obras/contratos/4 CONTRATO CALLE 1 ETAPA SAN JOSE RETUMBADERO (1).pdf', facturas: ''},
  {obras: 'CALLE ACUEDUCTO 1 ETAPA SAN PEDRO SOCHIAPAM', contratos: 'assets/pdfs/obras/contratos/3 CALLE ACUEDUCTO 1 ETAPA SAN PEDRO SOCHIAPAM.pdf', facturas: ''},
  {obras: 'Contrato KYSHA Servicios del Valle S.A. de C.V.', contratos: 'assets/pdfs/obras/contratos/3.pdf', facturas:  ''},
  {obras: 'CONSTRUCCIÓN DE SISTEMA DE AGUA POTABLE 1RA. ETAPA SAN JUAN ZAUTLA', contratos: 'assets/pdfs/obras/contratos/5 CONSTRUCCIÓN DE SISTEMA DE AGUA POTABLE 1RA. ETAPA SAN JUAN ZAUTLA.pdf', facturas: ''},
  {obras: 'CONSTRUCCIÓN DE PAVIMENTO DE CONCRETO HIDRÁULICO EN CALLE PRINCIPAL 1RA. EPA SAN JUAN ZAUTLA', contratos: 'assets/pdfs/obras/contratos/6 CONSTRUCCIÓN DE PAVIMENTO DE CONCRETO HIDRÁULICO EN CALLE PRINCIPAL 1RA. EPA SAN JUAN ZAUTLA.pdf', facturas: ''},
  {obras: 'Construcción de camino Km 0+0 al 0+13 1ra.Epa de Quetzalapa', contratos: 'assets/pdfs/obras/contratos/7 Construcción de camino Km 0+0  al 0+13 1ra.Epa de Quetzalapa.pdf', facturas:  ''},
  {obras: 'REHABILITACIÓN DEL SISTEMA DE AGUA POTABLE DE SAN PEDRO SOCHIAPAM', contratos: 'assets/pdfs/obras/contratos/9 REHABILITACIÓN DEL SISTEMA DE AGUA POTABLE DE SAN PEDRO SOCHIAPAM.pdf', facturas: ''},
  {obras: 'CONSTRUCCIÓN DE CANALETA DE AGUA PLUVIAL EN LA CALLE ACUEDUCTO SAN PEDRO SOCHIAPAM', contratos: 'assets/pdfs/obras/contratos/10 CONSTRUCCIÓN DE CANALETA DE AGUA PLUVIAL EN LA CALLE ACUEDUCTO SAN PEDRO SOCHIAPAM.pdf', facturas: ''},
  {obras: 'CONSTRUCCIÓN DE PISO FIRME', contratos: 'assets/pdfs/obras/contratos/11 CONSTRUCCIÓN DE VIVIENDA CUARTO DORMITORIO SANTIAGO QUETZALAPA.pdfpdfs/obras/contratos/12 CONSTRUCCIÓN DE PISO FIRME.pdf', facturas:  ''},
  {obras: 'CONSTRUCCIÓN DE VIVIENDA CUARTO DORMITORIO SANTIAGO QUETZALAPA', contratos: 'assets/pdfs/obras/contratos/11 CONSTRUCCIÓN DE VIVIENDA CUARTO DORMITORIO SANTIAGO QUETZALAPA.pdf', facturas: ''},
  {obras: 'CONTRATO DE AGUAPOTABLE FINCA MOCTEZUMA', contratos: 'assets/pdfs/obras/contratos/8 CONTRATOAGUAPOTABLEFINCAMOCTEZUMA.pdf', facturas: ''}
];
@Component({
  selector: 'app-tabla-contrato-obs',
  templateUrl: './tabla-contrato-obs.component.html',
  styleUrls: ['./tabla-contrato-obs.component.css']
})
export class TablaContratoOBSComponent implements OnInit {
  displayedColumns: string[] = ['obras', 'contratos', 'facturas'];
  dataSource = ELEMENT_DATA;
  constructor() { }

  ngOnInit() {
  }

}



