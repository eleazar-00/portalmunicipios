import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaContratoOBSComponent } from './tabla-contrato-obs.component';

describe('TablaContratoOBSComponent', () => {
  let component: TablaContratoOBSComponent;
  let fixture: ComponentFixture<TablaContratoOBSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaContratoOBSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaContratoOBSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
