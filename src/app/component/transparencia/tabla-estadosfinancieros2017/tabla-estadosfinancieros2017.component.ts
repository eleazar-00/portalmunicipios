import { Component, OnInit } from '@angular/core';
export interface estados2017 {
  estado: string;
  uno: string;
  dos: string;
  tres: string;
  cuatro: string;
}
const ELEMENT_DATA_EF2017: estados2017[] = [
  {estado: 'Balanza de Comprobación', uno: 'assets/pdfs/2017/Estado Financiero 1/4.- Balanza de Comprobacion.pdf', dos: 'assetspdfs/pdfs/2017/Estado Financiero 2/4 Balanza de Comprobación.pdf', tres: 'assets/pdfs/2017/Estado Financiero 3/4 Balanza de Comprobación.pdf',cuatro: 'assets/pdfs/2017/Estado Financiero 4/4 Balanza de Comprobación.pdf'},
  {estado: 'Balanza de Comprobación al cierre', uno: '', dos: '', tres: '',cuatro: 'assets/pdfs/2017/Estado Financiero 4/Balanza de Comprobación de Cierre.pdf'},
  {estado: 'Estado de Actividades', uno:'assets/pdfs/2017/Estado Financiero 1/3.- Estado de Actividades..pdf', dos:'assets/pdfs/2017/Estado Financiero 2/3 Edo de Actividades.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/3 Edo de Actividades.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/3 Edo de Actividades.pdf'},
  {estado: 'Estado de Cambios en la Situación Financiera', uno:'assets/pdfs/2017/Estado Financiero 1/6.- Estado de cambio en la Situación Financiera.pdf', dos:'assets/pdfs/2017/Estado Financiero 2/6 Edo de Cambio en la Situacion Financiera.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/6 Edo de Cambio en la Situacion Financiera.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/6 Edo de Cambio en la Situacion Financiera.pdf'},
  {estado: 'Estado de Flujo de Efectivo', uno:'assets/pdfs/2017/Estado Financiero 1/7.- Estado de flujo de efectivo.pdf', dos:'assets/pdfs/2017/Estado Financiero 2/7 Edo de Flujo de Efectivo.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/7 Edo de Flujo de Efectivo.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 3/7 Edo de Flujo de Efectivo.pdf'},

  {estado: 'Estado de Situación Financiera', uno:'assets/pdfs/2017/Estado Financiero 1/1.- Estado de Situación Financiera..pdf', dos:'assets/pdfs/2017/Estado Financiero 2/1 Estado de Situación Financiera.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/1 Estado de Situación Financiera.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/1 Estado de Situación Financiera.pdf'},
  {estado: 'Estado de Variación en la Hacienda Pública', uno:'assets/pdfs/2017/Estado Financiero 1/5.- Estado de Variación en la Hacienda Pública.pdf', dos:'assets/pdfs/2017/Estado Financiero 2/5 Estado de Variación en la Hacienda.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/5 Estado de Variación en la Hacienda.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/5 Estado de Variación en la Hacienda.pdf'},
  {estado: 'Estado del Ejercicio del Presupuesto de Egresos por Capitulo del Gasto', uno:'assets/pdfs/2017/Estado Presupuestal  1/2.- Estado del Ejercicio del Presupuesto de Egresos por Capítulo del Gasto.pdf', dos:'assets/pdfs/2017/Estado Presupuestal 2/2 Edo Analitico de Ejercicio del Presupuesto de Egreso por capitulo del Gasto.pdf', tres:'assets/pdfs/2017/Estado Presupuestal 3/2 Edo Analitico de Ejercicio del Presupuesto de Egreso por capitulo del Gasto.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/2 Edo Analitico de Ejercicio del Presupuesto de Egreso por capitulo del Gasto.pdf'},
  {estado: 'Estado del Ejercicio del Presupuesto de Egresos por Clasificación Administrativa', uno:'assets/pdfs/2017/Estado Presupuestal  1/3.- Estado del Ejercicio del Presupuestp de Egresos por Clasificación Administrativa.pdf', dos:'assets/pdfs/2017/Estado Presupuestal 2/3 Edo Analitico de Ejercicio del Presupuesto de Egreso por Clasificación Adminidtrativa.pdf', tres:'assets/pdfs/2017/Estado Presupuestal 3/3 Edo Analitico de Ejercicio del Presupuesto de Egreso por Clasificación Adminidtrativa.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/3 Edo Analitico de Ejercicio del Presupuesto de Egreso por Clasificación Adminidtrativa.pdf'},
  {estado: 'Estado del Ejercicio del Presupuesto de Egresos por Clasificación Económica', uno:'assets/pdfs/2017/Estado Presupuestal  1/5.- Estado del Ejercicio del Presupuesto de Egresos por Clasificación Económica.pdf', dos:'assets/pdfs/2017/Estado Presupuestal 2/5 Edo Analitico de Ejercicio del Presupuesto de Egreso por Clasificación Económica.pdf', tres:'assets/pdfs/2017/Estado Presupuestal 3/5 Edo Analitico de Ejercicio del Presupuesto de Egreso por Clasificación Económica.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/5 Edo Analitico de Ejercicio del Presupuesto de Egreso por Clasificación Económica.pdf'},

  {estado: 'Estado del Ejercicio del Presupuesto de Egresos por Clasificación Funcional', uno:'assets/pdfs/2017/Estado Presupuestal  1/4.- Estado del Ejercicio del Presupuesto de Egresos por Clasificación Funcional.pdf', dos:'assets/pdfs/2017/Estado Presupuestal 2/4 Edo Analitico de Ejercicio del Presupuesto de Egreso por Clasificación Funcional.pdf', tres:'assets/pdfs/2017/Estado Presupuestal 3/4 Edo Analitico de Ejercicio del Presupuesto de Egreso por Clasificación Funcional.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/4 Edo Analitico de Ejercicio del Presupuesto de Egreso por Clasificación Funcional.pdf'},
  {estado: 'Gasto por Categoría Programática', uno:'assets/pdfs/2017/Estado Presupuestal  1/7.- Gasto por Categoría Programática.pdf', dos:'assets/pdfs/2017/Estado Presupuestal 2/7 Gasto por Gategoría Programática.pdf', tres:'assets/pdfs/2017/Estado Presupuestal 3/7 Gasto por Gategoría Programática.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/7 Gasto por Gategoría Programática.pdf'},
  {estado: 'Estado Analítico del Activo', uno:'assets/pdfs/2017/Estado Financiero 1/2.- Estado Analitico del Activo..pdf', dos:'assets/pdfs/2017/Estado Financiero 2/2 Edo Anal del Activo.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/2 Edo Anal del Activo.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/2 Edo Anal del Activo.pdf'},
  {estado: 'Estado Analítico de la Deuda', uno:'assets/pdfs/2017/Estado Financiero 1/8.- Estado analitico de la Deuda Publica.pdf', dos:'assets/pdfs/2017/Estado Financiero 2/9 Edo Analitico de la Deuda.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/9 Edo Analitico de la Deuda.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/9 Edo Analitico de la Deuda.pdf'},
  {estado: 'Anexos de la Nota de Gestión', uno:'', dos:'assets/pdfs/2017/Estado Financiero 2/Anexos de la Nota de Gestión.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/Anexos de la Nota de Gestión.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/Anexos de la Nota de Gestión.pdf'},

  {estado: 'Nota al Estado de Actividades', uno:'', dos:'assets/pdfs/2017/Estado Financiero 2/Nota al Edo de Actividades.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/Nota al Edo de Actividades.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/Nota al Edo de Actividades.pdf'},
  {estado: 'Nota al Estado de Flujo de Efectivo', uno:'', dos:'assets/pdfs/2017/Estado Financiero 2/Nota al edo de Flujo.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/Nota al edo de Flujo.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/Nota al edo de Flujo.pdf'},
  {estado: 'Nota de Gestión', uno:'', dos:'assets/pdfs/2017/Estado Financiero 2/Nota de Gestion.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/Nota de Gestion.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/Nota de Gestion.pdf'},
  {estado: 'Nota de Memoria', uno:'', dos:'assets/pdfs/2017/Estado Financiero 2/Nota de Memoria.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/Nota de Memoria.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/Nota de Memoria.pdf'},
  {estado: 'Nota de Variación', uno:'', dos:'assets/pdfs/2017/Estado Financiero 2/Nota de Variación.pdf', tres:'assets/pdfs/2017/Estado Financiero 3/Nota de Variación.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/Nota de Variación.pdf'},


  {estado: 'Nota al Estado de Situación', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Financiero 3/Nota al Estado de Situación.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/Nota al Estado de Situación.pdf'},
  {estado: 'Nota al Estado de Situación', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Financiero 3/Anexos de la Nota de Gestión.pdf',cuatro:'assets/pdfs/2017/Estado Financiero 4/Anexos de la Nota de Gestión.pdf'},

  {estado: 'Conciliación entre los Ingresos Presupuestales y Contables', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Presupuestal 3/Conciliación entre los Ingresos Presupuestales y Contables.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/Conciliación entre los Ingresos Presupuestales y Contables.pdf'},
  {estado: 'Conciliación entre los Egresos Presupuestales y Contables', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Presupuestal 3/Conciliación entre los Egresos Presupuestales y Contables.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/Conciliación entre los Egresos Presupuestales y Contables.pdf'},
  {estado: 'Edo Analitico de Ingresos por Concepto', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Presupuestal 3/Edo Analitico de Ingresos por Concepto.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/Edo Analitico de Ingresos por Concepto.pdf'},
  {estado: 'Estado de Situación Financiera Original', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Presupuestal 3/Estado de Situación Financiera Original.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/Estado de Situación Financiera Original.pdf'},

  {estado: 'Endeudamiento', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Presupuestal 3/Endeudamiento.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/Endeudamiento.pdf'},
  {estado: 'Formato del Ejercicio y Destino del Gasto Federalizado', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Presupuestal 3/Formato del Ejercicio y Destino.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/Formato del Ejercicio y Destino.pdf'},
  {estado: 'Intereses de la Deuda', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Presupuestal 3/Intereses de la Deuda.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/Intereses de la Deuda.pdf'},
  {estado: 'Oficio del Acuse', uno:'', dos:'', tres:'assets/pdfs/2017/Oficio del Acuse.pdf',cuatro:'assets/pdfs/2017/Oficio del Acuse.pdf'},
  {estado: 'Relación de Cuentas Bancarias', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Presupuestal 3/Relación de Cuentas Bancarias.pdf',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/Relación de Cuentas Bancarias.pdf'},

  {estado: 'Memoria Cuentas de Orden', uno:'', dos:'', tres:'',cuatro:'assets/pdfs/2017/Estado Presupuestal 4//Memoria Cuentas de Orden.pdf'},

];
@Component({
  selector: 'app-tabla-estadosfinancieros2017',
  templateUrl: './tabla-estadosfinancieros2017.component.html',
  styleUrls: ['./tabla-estadosfinancieros2017.component.css']
})
export class TablaEstadosfinancieros2017Component implements OnInit {
  displayedColumns: string[] = ['estado', 'uno', 'dos','tres','cuatro'];
  dataSourceEstados2017 = ELEMENT_DATA_EF2017;
  constructor() { }

  ngOnInit() {
  }

}
