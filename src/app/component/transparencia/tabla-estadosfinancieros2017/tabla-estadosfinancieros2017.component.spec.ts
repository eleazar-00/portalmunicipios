import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaEstadosfinancieros2017Component } from './tabla-estadosfinancieros2017.component';

describe('TablaEstadosfinancieros2017Component', () => {
  let component: TablaEstadosfinancieros2017Component;
  let fixture: ComponentFixture<TablaEstadosfinancieros2017Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaEstadosfinancieros2017Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaEstadosfinancieros2017Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
