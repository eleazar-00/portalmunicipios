import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaAvancegestion2017Component } from './tabla-avancegestion2017.component';

describe('TablaAvancegestion2017Component', () => {
  let component: TablaAvancegestion2017Component;
  let fixture: ComponentFixture<TablaAvancegestion2017Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaAvancegestion2017Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaAvancegestion2017Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
