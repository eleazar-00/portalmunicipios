import { Component, OnInit } from '@angular/core';


export interface avances2017 {
  avance: string;
  uno: string;
  dos: string;
  tres: string;
  cuatro: string;
}
const ELEMENT_DATA: avances2017[] = [
  {avance: 'Estado Analítico de Ingresos', uno: 'assets/pdfs/2017/Estado Presupuestal  1/1.- Estado Analitico de Ingresos.pdf', dos: 'assets/pdfs/2017/Estado Presupuestal 2/1 Edo Analitico de Ingreso.pdf', tres: 'assets/pdfs/2017/Avances de Gestion 3/1 REPORTE ANALITICO DE INGRESOS.pdf',cuatro: 'assets/pdfs/2017/Avances de Gestion 4/4TO INF ANAL DE INGRESO 2017.pdf'},
  {avance: 'Reporte Analítico de Egresos', uno: 'assets/pdfs/2017/Estado Presupuestal  1/6.- Reporte Analitico de Egresos.pdf', dos: 'assets/pdfs/2017/Estado Presupuestal 2/6 Reporte Analitico de Egreso.pdf', tres: 'assets/pdfs/2017/Avances de Gestion 3/2 REPORTE ANALITICO DE EGRESOS.pdf',cuatro: 'assets/pdfs/2017/Avances de Gestion 4/4TO INF ANALITIO DE EGRESO 2017.pdf'},
  {avance: 'Estado Analitico de la Deuda', uno:'', dos:'', tres:'assets/pdfs/2017/Avances de Gestion 3/3 ESTADO ANALITICO DE LA DEUDA.pdf',cuatro:'assets/pdfs/2017/Avances de Gestion 4/4TO INF DEUDA PUBLICA.pdf'},
  {avance: 'Indicadores de Gestion', uno:'', dos:'', tres:'assets/pdfs/2017/Avances de Gestion 3/4 INDICADORES DE GESTIÓN.pdf',cuatro:'assets/pdfs/2017/Avances de Gestion 4/4TO INF INDICADORES DE GESTION.pdf'},
  {avance: 'Indicadores de Cumplimiento', uno:'', dos:'', tres:'assets/pdfs/2017/Avances de Gestion 3/5 INDICADORES DE CUMPLIMIENTO.pdf',cuatro:'assets/pdfs/2017/Avances de Gestion 4/4TO INDICADORES DE CUMPLIMIENTO.pdf'},

  {avance: 'Objetivos Acciones y Metas', uno:'', dos:'', tres:'assets/pdfs/2017/Avances de Gestion 3/6 OBJETIVOS ACCIONES Y METAS.pdf',cuatro:'assets/pdfs/2017/Avances de Gestion 4/4TO ACCIONES Y METAS.pdf'},
  {avance: 'Relación de Obras en Proceso', uno:'', dos:'', tres:'assets/pdfs/2017/Avances de Gestion 3/7 RELACIONES DE OBRAS EN PROCESO.pdf',cuatro:'assets/pdfs/2017/Avances de Gestion 4/4TO INF RELACION OBRA.pdf'},
  {avance: 'Registro y Control del Impuesto', uno:'', dos:'', tres:'assets/pdfs/2017/Avances de Gestion 3/8 REGISTRO Y CONTROL DE IMPUESTO.pdf',cuatro:'assets/pdfs/2017/Avances de Gestion 4/4TO. REGISTRO CONTROL INPUESTO.pdf'},
  {avance: 'Registro y control de Informe SFU', uno:'', dos:'', tres:'assets/pdfs/2017/Avances de Gestion 3/9 REGISTRO Y CONTROL DE INFORME  SFU.pdf',cuatro:'assets/pdfs/2017/Avances de Gestion 4/4TO INF REGISTO Y CONTROL SFU.pdf'},
  {avance: 'Inventario de Bienes Inmuebles', uno:'', dos:'', tres:'assets/pdfs/2017/Avances de Gestion 3/10 INVENTARIO DE BIENES MUEBLES.pdf',cuatro:'assets/pdfs/2017/Avances de Gestion 4/4TO INVENTARIO BIENES MUEBLES.pdf'},

  {avance: 'Inventario de Bienes Inmuebles', uno:'', dos:'', tres:'assets/pdfs/2017/Avances de Gestion 3/11 INVENTARIO DE BIENE INMUEBLE.pdf',cuatro:'assets/pdfs/2017/Avances de Gestion 4/4TO INF BIENES INMUEBLE.pdf'},
  {avance: 'Relación de Bienes que Componen el Patrimonio', uno:'', dos:'', tres:'assets/pdfs/2017/Estado Presupuestal 3/Relación de Bienes que Componen el Patrimonio.pdf',cuatro:''},
  {avance: 'Oficio Avances de Gestión', uno:'', dos:'', tres:'',cuatro:'assets/pdfs/2017/Estado Presupuestal 4/Oficio Avances de Gestion.pdf'}

];


@Component({
  selector: 'app-tabla-avancegestion2017',
  templateUrl: './tabla-avancegestion2017.component.html',
  styleUrls: ['./tabla-avancegestion2017.component.css']
})
export class TablaAvancegestion2017Component implements OnInit {
  displayedColumns: string[] = ['avance', 'uno', 'dos','tres','cuatro'];
  dataSource1 = ELEMENT_DATA;
  constructor() { }

  ngOnInit() {
  }

}
