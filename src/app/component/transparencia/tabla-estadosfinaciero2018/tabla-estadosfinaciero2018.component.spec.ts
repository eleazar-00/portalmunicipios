import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaEstadosfinaciero2018Component } from './tabla-estadosfinaciero2018.component';

describe('TablaEstadosfinaciero2018Component', () => {
  let component: TablaEstadosfinaciero2018Component;
  let fixture: ComponentFixture<TablaEstadosfinaciero2018Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaEstadosfinaciero2018Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaEstadosfinaciero2018Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
