import { Component, OnInit } from '@angular/core';

export interface estados {
  ano:string;
  estado: string;
  uno: string;
  dos: string;
  tres: string;
  cuatro: string;
}

const ELEMENT_DATA: estados[] = [
  {ano:'2018',estado: 'Balanza de Comprobación', uno: 'assets/pdfs/2018/ESTADOS FINANCIEROS/1 BALANZA DE COMPROBACION 1ER.pdf', dos: 'assets/pdfs/2018/ESTADOS FINANCIEROS/1 BALANZA DE COMPROBACION 2DO.pdf', tres: 'assets/pdfs/3ER TRIM 2018/3ER TRIM BALANZA DE 2018.pdf',cuatro: 'assets/pdfs/4TO TRIM - 2018/4.- BALANZA DEL PERIODO.pdf'},
  {ano:'2018',estado: 'Balanza de Comprobación al cierre', uno: '', dos: '', tres: '',cuatro: 'assets/pdfs/4TO TRIM - 2018/4A.- BALANZA DEL CIERRE DEL EJERCICIO.pdf'},
  {ano:'2018',estado: 'Estado de Actividades', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/3 EDO DE ACTIVIDADES 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/3 EDO DE ACTIVIDADES 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3.- EDO DE ACTIVIDADES.pdf',cuatro:'assets/pdfs/3.- EDO DE ACTIVIDADES.pdf'},
  {ano:'2018',estado: 'Estado de Cambios en la Situación Financiera', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/4 EDO DE CAMBIO SITUA FINANCIERA 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/4 EDO DE CAMBIO SITUA FINANCIERA 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/6.- EDO DE CAMBIOS EN LA SITUACION FINANCIERA.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/6.- EDO DE CAMBIOS EN LA SITUACION FINANCIERA.pdf'},
  {ano:'2018',estado: 'Estado de Flujo de Efectivo', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/5 EDO DE FLUJO EFECTIVO 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/5 EDO DE FLUJO EFECTIVO 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/7.- EDO DE FLUJOS DE EFECTIVO.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/7.- EDO DE FLUJOS DE EFECTIVO.pdf'},

  {ano:'2018',estado: 'Estado de Situación Financiera', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/6 EDO DE SITUACION FINANCIERA 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/6 EDO DE SITUACION FINANCIERA 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3R TRIM Estado de Situación Financiera Detallado.xlsx',cuatro:'assets/pdfs/4TO TRIM - 2018/1.- ESTADO DE SITUACION FINANCIERA.pdf'},
  {ano:'2018',estado: 'Estado de Variación en la Hacienda Pública', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/7 EDO DE VARIACION HACIENDA PUBLICA 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/7 EDO DE VARIACION HACIENDA PUBLICA 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/5.- EDO DE VARIACION EN LA HACIENDA PÚBLICA.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/5.- EDO DE VARIACION EN LA HACIENDA PÚBLICA.pdf'},
  {ano:'2018',estado: 'Estado del Ejercicio del Presupuesto de Egresos por Capitulo del Gasto', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/8 CLASIFICACION DEL GASTO 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/8 CLASIFICACION DEL GASTO 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/2.- EDO DEL  PRESP DEL EJERC POR CAPITULO DEL GASTO.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/2.- EDO DEL PRESP DEL EJERC POR CAPITULO DEL GASTO.pdf'},
  {ano:'2018',estado: 'Estado del Ejercicio del Presupuesto de Egresos por Clasificación Administrativa', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/9 CLASIFICACION ADMINISTRATIVA 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/9 CLASIFICACION ADMINISTRATIVA 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3.- EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN ADMINISTRATIVA.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/3.- EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN ADMINISTRATIVA.pdf'},
  {ano:'2018',estado: 'Estado del Ejercicio del Presupuesto de Egresos por Clasificación Económica', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS10 CLASIFICACION POR ECONOMICA 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS10 CLASIFICACION POR ECONOMICA 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/5.- EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN ECONOMICA.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/5.-EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN ECONOMICA.pdf'},

  {ano:'2018',estado: 'Estado del Ejercicio del Presupuesto de Egresos por Clasificación Funcional', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/11 CLASIFICACION FUNCIONAL 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/11 CLASIFICACION FUNCIONAL 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/4.- EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN FUNCIONAL.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4.- EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN FUNCIONAL.pdf'},
  {ano:'2018',estado: 'Gasto por Categoría Programática', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/12 CATEGORIA PROGRAMATICA 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/12 CATEGORIA PROGRAMATICA 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/7.- GASTO POR CATEGORIA PROGRAMATICA.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/7.- GASTOS POR CAT PROGRAMATICA.pdf'},
  {ano:'2018',estado: 'Estado Analítico del Activo', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/13 EDO ANALI ACTIVO 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/13 EDO ANALI ACTIVO 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/2.- EDO ANALITICO DEL ACTIVO.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/2.- EDO ANALITICO DEL ACTIVO.pdf'},
  {ano:'2018',estado: 'Estado Analítico de la Deuda', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/14 EDO ANALI DEUDA 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/14 EDO ANALI DEUDA 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/9.- EDO ANALITICO DE LA DEUDA.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/9.- EDO ANALITICO DE LA DEUDA.pdf'},
  {ano:'2018',estado: 'Anexos de la Nota de Gestión', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/15 ANEXOS A LAS NOTAS DE GESTIÓN ADMVA 1ER.xlsx', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/15 ANEXOS A LAS NOTAS DE GESTIÓN ADMVA 2DO.xlsx', tres:'',cuatro:''},

  {ano:'2018',estado: 'Nota al Estado de Actividades', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/16 NOTAS AL EDO DE ACTIVIDADES 1ER.xlsx', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/16 NOTAS AL EDO DE ACTIVIDADES 2DO.xlsx', tres:'',cuatro:''},
  {ano:'2018',estado: 'Nota al Estado de Flujo de Efectivo', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/17 NOTAS AL EDO FLUJO DE EFECTIVO 1ER.xlsx', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/17 NOTAS AL EDO FLUJO DE EFECTIVO 2DO.xlsx', tres:'',cuatro:''},
  {ano:'2018',estado: 'Nota de Gestión', uno:'', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/18 NOTAS GESTION ADMVA 2DO.docx', tres:'',cuatro:''},
  {ano:'2018',estado: 'Nota de Memoria', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/19 NOTAS DE MEMORIA 1ER.xlsx', dos:'assets/2018/ESTADOS FINANCIEROS/19 NOTAS DE MEMORIA 2DO.xlsx', tres:'',cuatro:''},
  {ano:'2018',estado: 'Nota al Estado de Situación', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/20 NOTA DE EDO SITUACION FINANCIERA 1ER.xlsx', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/20 NOTA DE EDO SITUACION FINANCIERA 2DO.xlsx', tres:'',cuatro:''},

  {ano:'2018',estado: 'Nota de Variación', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/16 NOTAS AL EDO DE ACTIVIDADES 1ER.xlsx', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/21 NOTAS AL ESTDO DE VARIACIÓN HACIENDA PÚBLICA 1ER.xlsx', tres:'',cuatro:''},
  {ano:'2018',estado: 'Conciliación entre los Ingresos Presupuestales y Contables', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/22 CONCILIACION DE INGRESOS 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/22 CONCILIACION DE INGRESOS 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER TRIM CONCILIACION ENTRE LOS INGRESOS.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRIM Conciliación entre los Egresos.pdf'},
  {ano:'2018',estado: 'Conciliación entre los Egresos Presupuestales y Contables', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/23 CONCILIACION ENTRE EGRESOS 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/23 CONCILIACION ENTRE EGRESOS 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER TRIM CONCILIACIÓN ENTRES LOS INGRESOS.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRIM Conciliación entre los Egresos.pdf'},
  {ano:'2018',estado: 'Edo Analitico de Ingresos por Concepto', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/24 EDO ANAL INGRESOS POR CONCEPTO 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/24 EDO ANAL INGRESOS POR CONCEPTO 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER TRIM EDO ANALITICO DE ING POR CONCEPTO.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRIM EDO ANALITICO INGRESOS POR CONCEPTO.pdf'},
  {ano:'2018',estado: 'Estado de Situación Financiera Original', uno:'', dos:'', tres:'',cuatro:''},

  {ano:'2018',estado: 'Endeudamiento', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/25 ENDEUDAMIENTO 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/25 ENDEUDAMIENTO 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER TRIM ENDEUDAMIENTO NETO.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRIM ENDEUDAMIENTO NETO.pdf'},
  {ano:'2018',estado: 'Formato del Ejercicio y Destino del Gasto Federalizado', uno:'', dos:'', tres:'assets/pdfs/3ER TRIM 2018/07 NORMA 3er TRIM 2018  formato de ejercicio y destino de gasto federalizado.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/07 NORMA  4to TRIM 2018  formato de ejercicio y destino de gasto federalizado.pdf'},
  {ano:'2018',estado: 'Intereses de la Deuda', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/26 INTERESES DE LA DEUDA 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/26 INTERESES DE LA DEUDA 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER TRIM INTERESES DEUDA.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO INTERESES DE LA DEUDA.pdf'},
  {ano:'2018',estado: 'Oficio del Acuse', uno:'', dos:'', tres:'',cuatro:''},
  {ano:'2018',estado: 'Relación de Cuentas Bancarias', uno:'assets/pdfs/2018/ESTADOS FINANCIEROS/27 RELACION DE CUENTAS BANCARIAS 1ER.pdf', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/27 RELACION DE CUENTAS BANCARIAS 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER TRIM RELACION CTAS BANCARIAS PRODUCTIVAS.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/10 NORMA 4to TRIM 2018 relacion de cuentas bancarias.pdf'},

  {ano:'2018',estado: 'Memoria Cuentas de Orden', uno:'', dos:'', tres:'',cuatro:''},
  {ano:'2018',estado: 'Formato del Destino de Gasto Federalizado', uno:'', dos:'assets/pdfs/2018/ESTADOS FINANCIEROS/27 FORMATO DEL DESTINO DEL GASTO FEDERALIZADO 2DO.xlsx', tres:'',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TIRM GASTOS FEDERALIZADO.xlsx'},


  {ano:'2019',estado: 'Balanza de Comprobación', uno: 'assets/pdfs/EJERCICIO 2019/1ER TRIM BALANZA DE COMPROBACIÓN.pdf', dos: '', tres: '',cuatro: ''},
  {ano:'2019',estado: 'Balanza de Comprobación al cierre', uno: '', dos: '', tres: '',cuatro: ''},
  {ano:'2019',estado: 'Estado de Actividades', uno:'assets/pdfs/EJERCICIO 2019/2 Notas al Estado de Actividades.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Estado de Cambios en la Situación Financiera', uno:'assets/pdfs/EJERCICIO 2019/5 1ER TRIM EDO DE CAMBIOS EN LA SITUACION FINANCIERA 2019.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Estado de Flujo de Efectivo', uno:'assets/pdfs/EJERCICIO 2019/6 1ER TRIM EDO DE FLUJOS DE EFECTIVO 2019.pdf', dos:'', tres:'',cuatro:''},

  {ano:'2019',estado: 'Estado de Situación Financiera', uno:'assets/pdfs/EJERCICIO 2019/1 1ER TRIM EDO DE SITUACION FINANCIERA 2019.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Estado de Variación en la Hacienda Pública', uno:'assets/pdfs/EJERCICIO 2019/4 1ER TRIM EDO DE VARIACION EN LA HACIENDA PÚBLICA 2019.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Estado del Ejercicio del Presupuesto de Egresos por Capitulo del Gasto', uno:'assets/pdfs/EJERCICIO 2019/2 1ER TRIM EDO DEL PRESP DEL EJERC POR CAPITULO DEL GASTO.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Estado del Ejercicio del Presupuesto de Egresos por Clasificación Administrativa', uno:'assets/pdfs/EJERCICIO 2019/3 1ER TRIM EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN ADMINISTRATIVA.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Estado del Ejercicio del Presupuesto de Egresos por Clasificación Económica', uno:'assets/pdfs/EJERCICIO 2019/5 1ER TRIM EDO DE PRESP DEL EJER POR CLASIFICACIÓN ECONÓMICA.pdf', dos:'', tres:'',cuatro:''},

  {ano:'2019',estado: 'Estado del Ejercicio del Presupuesto de Egresos por Clasificación Funcional', uno:'assets/pdfs/EJERCICIO 2019/4 1ER TRIM EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN FUNCIONAL.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Gasto por Categoría Programática', uno:'assets/pdfs/EJERCICIO 2019/GASTOS PROGRAMÁTICA.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Estado Analítico del Activo', uno:'assets/pdfs/EJERCICIO 2019/2 1ER TRIM EDO ANALITICO DEL ACTIVO 2019.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Estado Analítico de la Deuda', uno:'assets/pdfs/EJERCICIO 2019/7 1ER TRIM EDO ANALITICO DEUDA Y OTROS PASIVO 2019.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Anexos de la Nota de Gestión', uno:'assets/pdfs/EJERCICIO 2019/7 Notas de Gestión Administrativa (Anexos.pdf', dos:'', tres:'',cuatro:''},

  {ano:'2019',estado: 'Nota al Estado de Actividades', uno:'assets/pdfs/EJERCICIO 2019/2 Notas al Estado de Actividades.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Nota al Estado de Flujo de Efectivo', uno:'assets/pdfs/EJERCICIO 2019/4 Notas Estado de Flujos de Efectivo.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Nota de Gestión', uno:'assets/pdfs/EJERCICIO 2019/6 Notas de Gestión Administrativa.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Nota de Memoria', uno:'assets/pdfs/EJERCICIO 2019/5 Notas de Memoria.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Nota al Estado de Situación', uno:'assets/pdfs/EJERCICIO 2019/1 Notas al Estado de Situación Financiera.pdf', dos:'', tres:'',cuatro:''},

  {ano:'2019',estado: 'Nota de Variación', uno:'assets/pdfs/EJERCICIO 2019/3 Notas al Estado de Variación en la Hacienda Pública.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Conciliación entre los Ingresos Presupuestales y Contables', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM CONCILIACIÓN ENTRE LOS INGRESOS.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Conciliación entre los Egresos Presupuestales y Contables', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM CONCILIACIÓN ENTRE LOS EGRESOS.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Edo Analitico de Ingresos por Concepto', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM ESTADO ANALÍTICO DE INGRESOS POR CONCEPTO.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Estado de Situación Financiera Original', uno:'assets/pdfs/EJERCICIO 2019/5 1ER TRIM EDO DE CAMBIOS EN LA SITUACION FINANCIERA 2019.pdf', dos:'', tres:'',cuatro:''},

  {ano:'2019',estado: 'Endeudamiento', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM ENDEUDAMIENTO NETO.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Formato del Ejercicio y Destino del Gasto Federalizado', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM EJERCICIO Y DESTINO DE GASTO FEDERALIZADO Y REINTEGROS..pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Intereses de la Deuda', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM INTERESES DE LA DEUDA.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Oficio del Acuse', uno:'assets/pdfs/EJERCICIO 2019/ACUSE DE INF. PULICADO A CONAC.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Relación de Cuentas Bancarias', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM RELACIÓN DE CUENTAS BANCARIAS PRODUCTIVAS ESPECÍFICAS.pdf', dos:'', tres:'',cuatro:''},

  {ano:'2019',estado: 'Memoria Cuentas de Orden', uno:'', dos:'', tres:'',cuatro:''},
  {ano:'2019',estado: 'Formato del Destino de Gasto Federalizado', uno:'', dos:'', tres:'',cuatro:''}

];


@Component({
  selector: 'app-tabla-estadosfinaciero2018',
  templateUrl: './tabla-estadosfinaciero2018.component.html',
  styleUrls: ['./tabla-estadosfinaciero2018.component.css']
})
export class TablaEstadosfinaciero2018Component implements OnInit {
  displayedColumns: string[] = ['ano','estado', 'uno', 'dos','tres','cuatro'];
  dataSourceEstados = ELEMENT_DATA;
  constructor() { }

  ngOnInit() {
  }

}
