import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaReporte2017Component } from './tabla-reporte2017.component';

describe('TablaReporte2017Component', () => {
  let component: TablaReporte2017Component;
  let fixture: ComponentFixture<TablaReporte2017Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaReporte2017Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaReporte2017Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
