import { Component, OnInit } from '@angular/core';
export interface reportes2017 {
  reporte: string;
  uno: string;
  dos: string;
  tres: string;
  cuatro: string;
}

const ELEMENT_DATA: reportes2017[] = [
  {reporte: 'Ayudas', uno: 'assets/pdfs/2017/Normas 1/Montos pagados por ayudas y subsidios.pdf', dos: 'assets/pdfs/2017/Normas 2/Montos pagados por ayudas y subsidios.pdf', tres: '',cuatro: ''},
  {reporte: 'Formato de Programas con Recursos Concurrente por Orden de Gobierno', uno: 'assets/pdfs/2017/Normas 1/Formato de Programas con Recursos Concurrente por orden de gobierno.pdf', dos: 'assets/pdfs/2017/Normas 2/Formato de Programas con Recursos Concurrente por orden de gobierno.pdf', tres: '',cuatro: ''},
  {reporte: 'Relación de Cuentas Bancarias Productivas Específicas', uno:'assets/pdfs/2017/Normas 1/Relación de Cuentas Bancarias Productivas Especificas.pdf', dos:'assets/pdfs/2017/Normas 2/Relación de Cuentas Bancarias Productivas Especificas.pdf', tres:'',cuatro:''},
  {reporte: 'Formato de Información de Aplicación de Recursos del FORTAMUN', uno:'assets/pdfs/2017/Normas 1/Formato de Información de Aplicación de recursos del FORTAMUN.pdf', dos:'assets/pdfs/2017/Normas 2/Formato de Información de Aplicación de recursos del FORTAMUN.pdf', tres:'',cuatro:''},
  {reporte: 'Formato de Información de Obligaciones Pagadas o garantizadas con Fondos Federales', uno:'assets/pdfs/2017/Normas 1/Formato de Información de Obligaciones pagadas o garantizadas con Federales.pdf', dos:'assets/pdfs/2017/Normas 2/Formato de Información de Obligaciones pagadas o garantizadas con Federales DEUDA.pdf', tres:'',cuatro:''},

  {reporte: 'Formato del Ejercicio y Destino de Gasto Federalizado y Reintegros', uno:'assets/pdfs/2017/Normas 1/Formato del ejercicio y Destino de Gasto Federalizado y Reintegros.pdf', dos:'assets/pdfs/2017/Normas 2/Formato del ejercicio y Destino de Gasto Federalizado y Reintegros.pdf', tres:'',cuatro:''},
  {reporte: 'Formato para la Difusión de los Resultados de las Evaluaciones', uno:'assets/pdfs/2017/Normas 1/Formato para la Difusión de los Resultados de las Evaluaciones.pdf', dos:'assets/pdfs/2017/Normas 2/Formato para la Difusión de los Resultados de las Evaluaciones.pdf', tres:'',cuatro:''},
  {reporte: 'Información Pública Financiera para el Fondo de Aportaciones para la Infraestructura Social', uno:'', dos:'assets/pdfs/2017/Normas 2/Información Publica Financiera para el Fondo de Aportaciones para la Infraestructura Social.pdf', tres:'',cuatro:''}
];

@Component({
  selector: 'app-tabla-reporte2017',
  templateUrl: './tabla-reporte2017.component.html',
  styleUrls: ['./tabla-reporte2017.component.css']
})
export class TablaReporte2017Component implements OnInit {
  displayedColumns: string[] = ['reporte', 'uno', 'dos','tres','cuatro'];
  dataSourceReporte2017 = ELEMENT_DATA;
  constructor() { }

  ngOnInit() {
  }

}
