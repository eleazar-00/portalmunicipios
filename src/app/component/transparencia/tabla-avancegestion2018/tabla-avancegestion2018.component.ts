import { Component, OnInit } from '@angular/core';
export interface avances {
  avance: string;
  uno: string;
  dos: string;
  tres: string;
  cuatro: string;
  ano:string;
}

const ELEMENT_DATA: avances[] = [
  {avance: 'Estado Analítico de Ingresos', uno: 'assets/pdfs/2018/AVANCE DE GESTION/1 REPORTE ANALI INGRESOS 1ER.pdf', dos: 'assets/pdfs/2018/AVANCE DE GESTION/1 REPORTE ANALI INGRESOS 2DO.pdf', tres: 'assets/pdfs/3ER TRIM 2018/1.- EDO ANALITICO DE INGRESOS.pdf',cuatro: 'assets/pdfs/4TO TRIM - 2018/1.- EDO ANALITICO DE INGRESOS.pdf',ano:'2018'},
  {avance: 'Reporte Analítico de Egresos', uno: 'assets/pdfs/2018/AVANCE DE GESTION/2 REPORTE ANALI EGRESOS 1ER.pdf', dos: 'assets/pdfs/2018/AVANCE DE GESTION/2 REPORTE ANALI EGRESOS 2DO.pdf', tres: 'assets/pdfs/3ER TRIM 2018/6.- REPORTE ANALITICO DE EGRESOS.pdf',cuatro: 'assets/pdfs/4TO TRIM - 2018/6.- REPORTE ANALITICO DE EGRESOS.pdf',ano:'2018'},
  {avance: 'Estado Analitico de la Deuda', uno:'assets/pdfs/2018/AVANCE DE GESTION/3 REPORTE ANALI DEUDA 1ER.pdf', dos:'assets/pdfs/2018/AVANCE DE GESTION/3 REPORTE ANALI DEUDA 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/9.- EDO ANALITICO DE LA DEUDA.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/9.- EDO ANALITICO DE LA DEUDA.pdf',ano:'2018'},
  {avance: 'Indicadores de Gestion', uno:'assets/pdfs/2018/AVANCE DE GESTION/4-INDICADORE DE GESTION 1ER TRI.pdf', dos:'assets/pdfs/2018/AVANCE DE GESTION/4-INDICADORE DE GESTION 2DO TRI.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER TRIM INDICADORES DE GESTION.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRI Indicadores de Gestión.pdf',ano:'2018'},
  {avance: 'Indicadores de Cumplimiento', uno:'assets/pdfs/2018/AVANCE DE GESTION/5 INDICADORES DE CUMPLIMIENTO 1ER.pdf', dos:'assets/pdfs/2018/AVANCE DE GESTION/5 INDICADORES DE CUMPLIMIENTO 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER INDICADORES DE CUMPLIMIENTO.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO Indicadores de Cumplimiento de Objetivos.pdf',ano:'2018'},
  {avance: 'Relación de Obras en Proceso', uno:'assets/pdfs/2018/AVANCE DE GESTION/7 RELACION DE OBRAS 1 ER.pdf', dos:'assets/pdfs/2018/AVANCE DE GESTION/7 RELACION DE OBRAS 2 DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER RELACION DE OBRAS EN PROCESO.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRIM Relación de Obras en Proceso y Terminadas.pdf',ano:'2018'},
  {avance: 'Registro y Control del Impuesto', uno:'assets/pdfs/2018/AVANCE DE GESTION/8 REGISTRO Y CONTROL DE IMPUESTO 1ER.pdf', dos:'assets/pdfs/2018/AVANCE DE GESTION/8 REGISTRO Y CONTROL DE IMPUESTO 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER REGISTRO Y CONTROL DE IMPUESTO.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRIM Registro y Control del Impuesto.pdf',ano:'2018'},
  {avance: 'Registro y control de Informe SFU', uno:'assets/pdfs/2018/AVANCE DE GESTION/9 REGISTRO Y CONTROL SFU 1ER.pdf', dos:'assets/pdfs/2018/AVANCE DE GESTION/9 REGISTRO Y CONTROL SFU 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER REGISTRO Y CONTROL DE SFU.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRIM Registro y control de Informe SFU.pdf',ano:'2018'},
  {avance: 'Inventario de Bienes Muebles', uno:'assets/pdfs/2018/AVANCE DE GESTION/10 INVENTARIO BIENES MUEBLE 1ER.pdf', dos:'assets/pdfs/2018/AVANCE DE GESTION/10 INVENTARIO BIENES MUEBLE 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER INVENTARIOS DE BIENES MUEBLES.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRIM INVENTARIO DE BIENES MUEBLES.pdf',ano:'2018'},
  {avance: 'Inventario de Bienes Inmuebles', uno:'assets/pdfs/2018/AVANCE DE GESTION/11 INVENTARIO INMUEBLES 1ER.pdf', dos:'assets/pdfs/2018/AVANCE DE GESTION/11 INVENTARIO INMUEBLES 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER INVENTARIO DE BIENES INMUEBLE.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO TRIM INVENTARIO DE BIENES INMUEBLES.pdf',ano:'2018'},
  {avance: 'Relación de Bienes que Componen el Patrimonio', uno:'assets/pdfs/2018/AVANCE DE GESTION/12 INVENTARIO PATRIMONI MUNIC 1ER.pdf', dos:'assets/pdfs/2018/AVANCE DE GESTION/12 INVENTARIO PATRIMONI MUNIC 2DO.pdf', tres:'assets/pdfs/3ER TRIM 2018/3ER INVENTARIO DE BIENES QUE COMPONE PATRIMONIAL.pdf',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO Relación de Bienes que Componen el Patrimoni.pdf',ano:'2018'},

  {avance: 'Estado Analítico de Ingresos', uno: 'assets/pdfs/EJERCICIO 2019/1 1ER TRIM EDO ANALITICO DE INGRESOS.pdf', dos: '', tres: '',cuatro: '',ano:'2019'},
  {avance: 'Reporte Analítico de Egresos', uno: 'assets/pdfs/EJERCICIO 2019/1ER TRIM REPORTE ANALITICO DE EGRESOS.pdf', dos: '', tres: '',cuatro: '',ano:'2019'},
  {avance: 'Estado Analitico de la Deuda', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM INTERESES DE LA DEUDA.pdf', dos:'', tres:'',cuatro:'',ano:'2019'},
  {avance: 'Indicadores de Gestion', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM Indicadores de Gestion.pdf', dos:'', tres:'',cuatro:'',ano:'2019'},
  {avance: 'Indicadores de Cumplimiento', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM INDICADORES DE CUMPLIMIENTO DE OBJETIVOS.pdf', dos:'', tres:'',cuatro:'',ano:'2019'},
  {avance: 'Relación de Obras en Proceso', uno:'assets/pdfs/EJERCICIO 2019/1 ER TRIM RELACION DE LAS OBRAS.pdf', dos:'', tres:'',cuatro:'',ano:'2019'},
  {avance: 'Registro y Control del Impuesto', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM REGISTRO Y CONTROL DE IMPUESTOS.pdf', dos:'', tres:'',cuatro:'',ano:'2019'},
  {avance: 'Registro y control de Informe SFU', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM REGISTRO Y CONTROL DE INFORMES SFU SHCP.pdf', dos:'', tres:'',cuatro:'',ano:'2019'},
  {avance: 'Inventario de Bienes Muebles', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM INVENTARIO DE BIENES MUEBLES.pdf', dos:'', tres:'',cuatro:'',ano:'2019'},
  {avance: 'Inventario de Bienes Inmuebles', uno:'assets/pdfs/EJERCICIO 2019/1ER TRIM INVENTARIO DE BIENES INMUEBLE.pdf', dos:'', tres:'',cuatro:'',ano:'2019'},
  {avance: 'Relación de Bienes que Componen el Patrimonio', uno:'', dos:'', tres:'',cuatro:'',ano:'2019'}
];

@Component({
  selector: 'app-tabla-avancegestion2018',
  templateUrl: './tabla-avancegestion2018.component.html',
  styleUrls: ['./tabla-avancegestion2018.component.css']
})
export class TablaAvancegestion2018Component implements OnInit {
  displayedColumns: string[] = ['ano','avance', 'uno', 'dos','tres','cuatro'];
  dataSource = ELEMENT_DATA;
  constructor() { }

  ngOnInit() {
  }

}
