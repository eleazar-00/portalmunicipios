import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaAvancegestion2018Component } from './tabla-avancegestion2018.component';

describe('TablaAvancegestion2018Component', () => {
  let component: TablaAvancegestion2018Component;
  let fixture: ComponentFixture<TablaAvancegestion2018Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaAvancegestion2018Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaAvancegestion2018Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
