import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Anexos2018Component } from './anexos2018.component';

describe('Anexos2018Component', () => {
  let component: Anexos2018Component;
  let fixture: ComponentFixture<Anexos2018Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Anexos2018Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Anexos2018Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
