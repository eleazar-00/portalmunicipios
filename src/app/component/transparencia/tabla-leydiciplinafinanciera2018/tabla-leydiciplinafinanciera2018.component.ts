import { Component, OnInit } from '@angular/core';
export interface diciplina {
  ano:string;
  diciplina: string;
  uno: string;
  dos: string;
  tres: string;
  cuatro: string;
}

const ELEMENT_DATA: diciplina[] = [
  {ano:'2018',diciplina: 'Estado de Situación Financiera Detallada', uno: 'assets/pdfs/2018/DISCIPLINA FINANCIERA/1 EDO SITUAC FINANCIERA DETALLADA 1ER.xlsx', dos: 'assets/pdfs/2018/DISCIPLINA FINANCIERA/1 EDO SITUAC FINANCIERA DETALLADA 2DO.xlsx', tres: 'assets/pdfs/3ER TRIM 2018/3R TRIM Estado de Situación Financiera Detallado.xlsx',cuatro: 'assets/pdfs/4TO TRIM - 2018/4to Estado de Situación Financiera Detallado.xlsx'},
  {ano:'2018',diciplina: 'Información Analitica de la Deuda y Otros Pasivos', uno: 'assets/pdfs/2018/DISCIPLINA FINANCIERA/2 INFORME DEUDA PUBLICA Y OTROS 1ER.xlsx', dos: 'assets/pdfs/2018/DISCIPLINA FINANCIERA/2 INFORME DEUDA PUBLICA Y OTROS 2DO.xlsx', tres: 'assets/pdfs/3ER TRIM 2018/3R TRIM Informe Analítico de la Deuda y otros Pasivos.xlsx',cuatro: 'assets/pdfs/4TO TRIM - 2018/4to tri analitico de la deuda pública.xlsx'},
  {ano:'2018',diciplina: 'Información Analitica de Obligaciones Diferidas de Financiamiento', uno:'assets/pdfs/2018/DISCIPLINA FINANCIERA/3 INFORME ANALITICO FINANCIAMIENTO 1ER.xlsx', dos:'assets/pdfs/2018/DISCIPLINA FINANCIERA/3 INFORME ANALITICO FINANCIAMIENTO 2DO.xlsx', tres:'assets/pdfs/3ER TRIM 2018/3R INFORME ANALÍTICO DE OBLIGACIONES DIFERENTES DE FINANCIAMIENTOS.xlsx',cuatro:'assets/pdfs/4TO TRIM - 2018/4to Informe Analítico de Obligaciones Diferentes de Financiamientos.xlsx'},
  {ano:'2018',diciplina: 'Balance Presupuestario', uno:'assets/pdfs/2018/DISCIPLINA FINANCIERA/4 BALANCE PRESUPUESTARIO 1ER.xlsx', dos:'assets/pdfs/2018/DISCIPLINA FINANCIERA/4 BALANCE PRESUPUESTARIO 2DO.xlsx', tres:'assets/pdfs/3ER TRIM 2018/3R BALANCE PRESUPUESTARIO .xlsx',cuatro:'assets/pdfs/4TO TRIM - 2018/4to Balance Presupuestario.xlsx'},
  {ano:'2018',diciplina: 'Estado Analítico de Ingresos Detallado', uno:'assets/pdfs/2018/DISCIPLINA FINANCIERA/5 EDO ANALI INGRESO DETALLADO 1ER.xlsx', dos:'assets/pdfs/2018/DISCIPLINA FINANCIERA/5 EDO ANALI INGRESO DETALLADO 2DO.xlsx', tres:'assets/pdfs/3ER TRIM 2018/3ER TRIM ESTADO ANALÍTICO DE INGRESOS DETALLADO.xlsx',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO Estado Analítico de Ingresos Detallado.xlsx'},
  {ano:'2018',diciplina: 'Estado de Situación Financiera Detallado Clasificación de la Funcional', uno:'assets/pdfs/2018/DISCIPLINA FINANCIERA/6 PRESUPUESTO CLASIFICADO FUNCIONAL 1ER.xlsx', dos:'assets/pdfs/2018/DISCIPLINA FINANCIERA/6 PRESUPUESTO CLASIFICADO FUNCIONAL 2DO.xlsx', tres:'assets/pdfs/3ER TRIM 2018/3R EAEPE CLASIFICACION FUNCIONAL.xlsx',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO EAEPE CLASIFICACIÓN FUNCIONAL.xlsx'},
  {ano:'2018',diciplina: 'Estado de Situación Financiera Detallado Clasificación Administrativa', uno:'assets/pdfs/2018/DISCIPLINA FINANCIERA/7 EDO PRESUPUESTARIO ADMINISTRATIVA 1ER.xlsx', dos:'assets/pdfs/2018/DISCIPLINA FINANCIERA/7 EDO PRESUPUESTARIO ADMINISTRATIVA 2DO.xlsx', tres:'assets/pdfs/3ER TRIM 2018/3R TRIM  EAEPE  CLASIFICACION ADMINISTRATIVA.xlsx',cuatro:'assets/pdfs/4TO TRIM - 2018/4 TO EAEPE por clasificación administrativa.xlsx'},
  {ano:'2018',diciplina: 'Estado de Situación Financiera Detallado Clasificación Servicios Personales por Categoría', uno:'assets/pdfs/2018/DISCIPLINA FINANCIERA/8 POR SERVICIO PROFESIONALES X CATEGORIA 1ER.xlsx', dos:'assets/pdfs/2018/DISCIPLINA FINANCIERA/8 POR SERVICIO PROFESIONALES X CATEGORIA 2DO.xlsx', tres:'',cuatro:''},
  {ano:'2018',diciplina: 'Estado de Situación Financiera Detallado Clasificación por Objeto del Gasto', uno:'assets/pdfs/2018/DISCIPLINA FINANCIERA/9 PRESUPUESTO POR OBJETO DEL GASTO 1ER.xlsx', dos:'assets/pdfs/2018/DISCIPLINA FINANCIERA/9 PRESUPUESTO POR OBJETO DEL GASTO 2DO.xlsx', tres:'assets/pdfs/3ER TRIM 2018/3ER TRIM OBJETO DEL GASTOS.xlsx',cuatro:'assets/pdfs/4TO TRIM - 2018/4TO  EAEPE POR OBJETO DEL GASTO.xlsx'},

  {ano:'2019',diciplina: 'Estado de Situación Financiera Detallada', uno: '', dos: '', tres: '',cuatro: ''},
  {ano:'2019',diciplina: 'Información Analitica de la Deuda y Otros Pasivos', uno: 'assets/pdfs/EJERCICIO 2019/7 1ER TRIM EDO ANALITICO DEUDA Y OTROS PASIVO 2019.pdf', dos: '', tres: '',cuatro: ''},
  {ano:'2019',diciplina: 'Información Analitica de Obligaciones Diferidas de Financiamiento', uno:'', dos:'', tres:'',cuatro:''},
  {ano:'2019',diciplina: 'Balance Presupuestario', uno:'', dos:'', tres:'',cuatro:''},
  {ano:'2019',diciplina: 'Estado Analítico de Ingresos Detallado', uno:'assets/pdfs/EJERCICIO 2019/INGRESOS FEDERALES DETALLADO.pdf', dos:'',tres:'',cuatro:''},
  {ano:'2019',diciplina: 'Estado de Situación Financiera Detallado Clasificación de la Funcional', uno:'assets/pdfs/EJERCICIO 2019/4 1ER TRIM EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN FUNCIONAL.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',diciplina: 'Estado de Situación Financiera Detallado Clasificación Administrativa', uno:'assets/pdfs/EJERCICIO 2019/3 1ER TRIM EDO DEL PRESP DEL EJERC POR CLASIFICACIÓN ADMINISTRATIVA.pdf', dos:'', tres:'',cuatro:''},
  {ano:'2019',diciplina: 'Estado de Situación Financiera Detallado Clasificación Servicios Personales por Categoría', uno:'assets/pdfs/EJERCICIO 2019/', dos:'', tres:'',cuatro:''},
  {ano:'2019',diciplina: 'Estado de Situación Financiera Detallado Clasificación por Objeto del Gasto', uno:'assets/pdfs/EJERCICIO 2019/2 1ER TRIM EDO DEL PRESP DEL EJERC POR CAPITULO DEL GASTO.pdf', dos:'', tres:'',cuatro:''}
  ];
@Component({
  selector: 'app-tabla-leydiciplinafinanciera2018',
  templateUrl: './tabla-leydiciplinafinanciera2018.component.html',
  styleUrls: ['./tabla-leydiciplinafinanciera2018.component.css']
})
export class TablaLeydiciplinafinanciera2018Component implements OnInit {
  displayedColumns: string[] = ['ano','diciplina', 'uno', 'dos','tres','cuatro'];
  dataSourceDiciplina = ELEMENT_DATA;
  constructor() { }

  ngOnInit() {
  }

}
