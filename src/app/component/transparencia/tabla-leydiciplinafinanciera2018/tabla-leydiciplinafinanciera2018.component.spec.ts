import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaLeydiciplinafinanciera2018Component } from './tabla-leydiciplinafinanciera2018.component';

describe('TablaLeydiciplinafinanciera2018Component', () => {
  let component: TablaLeydiciplinafinanciera2018Component;
  let fixture: ComponentFixture<TablaLeydiciplinafinanciera2018Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaLeydiciplinafinanciera2018Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaLeydiciplinafinanciera2018Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
