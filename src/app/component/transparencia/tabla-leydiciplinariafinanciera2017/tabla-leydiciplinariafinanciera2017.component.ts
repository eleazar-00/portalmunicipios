import { Component, OnInit } from '@angular/core';

export interface diciplina2017 {
  diciplina: string;
  uno: string;
  dos: string;
  tres: string;
  cuatro: string;
}
const ELEMENT_DATA_DIS2017: diciplina2017[] = [
  {diciplina: 'Estado de Situación Financiera Detallada', uno: 'assets/pdfs/2017/Ley Disciplina Financiera 1/1 Estado de Situación Financiera Detallada.xlsx', dos: 'assets/pdfs/2017/Ley Disciplina Financiera 2/1.--ESTADO DE SITUACIÓN FINANCIERA DETALLADO.xlsx', tres: 'assets/pdfs/2017/Ley Disciplina Financiera 3/1 EDO FINANCIERA DETALLADA 3RO.pdf',cuatro: 'assets/pdfs/2017/Ley Disciplina Financiera 3/1 EDO FINANCIERA DETALLADA 4TO.pdf'},
  {diciplina: 'Información Analitica de la Deuda y Otros Pasivos', uno: 'assets/pdfs/2017/Ley Disciplina Financiera 1/2 Inf Analít de la Deuda y Otros Pasivos.xlsx', dos: 'assets/pdfs/2017/Ley Disciplina Financiera 2/2.---INFORME ANALÍTICO DE LA DEUDA PÚBLICA Y OTROS PASIVOS.xlsx', tres: 'assets/pdfs/2017/Ley Disciplina Financiera 3/2 INFORME ANAL DEUDA Y OTROS 3ER.xlsx',cuatro: 'assets/pdfs/2017/Ley Disciplina Financiera 3/2 INFORME ANAL DEUDA Y OTROS 4TO.xlsx'},
  {diciplina: 'Información Analitica de Obligaciones Diferidas de Financiamiento', uno: 'assets/pdfs/2017/Ley Disciplina Financiera 1/3  Inf Anal de Obligaciones Dife de Financiamientos.xlsx', dos: 'assets/pdfs/2017/Ley Disciplina Financiera 2/3.--Inf Anal de Obligaciones Difer de Financiamiento.xlsx', tres: 'assets/pdfs/2017/Ley Disciplina Financiera 3/3 INFORME ANALITICO FINANCIAMIENTO 3ER.xlsx',cuatro: 'assets/pdfs/2017/Ley Disciplina Financiera 3/3 INFORME ANALITICO FINANCIAMIENTO 4TO.xlsx'},
  {diciplina: 'Balance Presupuestario', uno: 'assets/pdfs/2017/Ley Disciplina Financiera 1/4   Balance Presupuestario.xlsx', dos: 'assets/pdfs/2017/Ley Disciplina Financiera 2/4.--- BALANCE PRESUPUESTARIO - LDF.xlsx', tres: 'assets/pdfs/2017/Ley Disciplina Financiera 3/4 BALANCE 3ER.pdf',cuatro: 'assets/pdfs/2017/Ley Disciplina Financiera 3/4 BALANCE 4TO.pdf'},
  {diciplina: 'Estado Analítico de Ingresos Detallado', uno: 'assets/pdfs/2017/Ley Disciplina Financiera 1/5   Estado Analítico de Ingresos Detallado.xlsx', dos: 'assets/pdfs/2017/Ley Disciplina Financiera 2/5.-- ESTADO ANALÍTICO DE INGRESOS DETALLADO.xlsx', tres: 'assets/pdfs/2017/Ley Disciplina Financiera 3/5 INGRESO DETALLADO 3ER.xlsx',cuatro: 'assets/pdfs/2017/Ley Disciplina Financiera 3/5 INGRESO DETALLADO 4TO.xlsx'},

  {diciplina: 'Estado de Situación Financiera Detallado Clasificación de la Funcional', uno: 'assets/pdfs/2017/Ley Disciplina Financiera 1/6  Estado de Situacion Financiera Detallado  CFG.xlsx', dos: 'assets/pdfs/2017/Ley Disciplina Financiera 2/6.A)-- Estado Ana del Ejer del Pres de Egresos Detallado CF.xlsx', tres: 'assets/pdfs/2017/Ley Disciplina Financiera 3/8 PRESUPUESTO CLASIFICACION FUNCIONAL 3ER.xlsx',cuatro: 'assets/pdfs/2017/Ley Disciplina Financiera 3/8 PRESUPUESTO CLASIFICACION FUNCIONAL 4TO.xlsx'},
  {diciplina: 'Estado de Situación Financiera Detallado Clasificación Administrativa', uno: 'assets/pdfs/2017/Ley Disciplina Financiera 1/6  Estado de Situacion Financiera Detallado CA.xlsx', dos: 'assets/pdfs/2017/Ley Disciplina Financiera 2/6.B)-- Estado Ana del Ejer del Pres de Egresos Detallado CA.xlsx', tres: 'assets/pdfs/2017/Ley Disciplina Financiera 3/7 CLASIFICACION ADMINISTRATIVA 3ER.xlsx',cuatro: 'assets/pdfs/2017/Ley Disciplina Financiera 3/7 CLASIFICACION ADMINISTRATIVA 4TO.xlsx'},
  {diciplina: 'Estado de Situación Financiera Detallado Clasificación Servicios Personales por Categoría', uno: 'assets/pdfs/2017/Ley Disciplina Financiera 1/6  Estado de Situacion Financiera Detallado CSPC.xlsx', dos: 'assets/pdfs/2017/Ley Disciplina Financiera 2/6.D)-- Estado Ana del Ejer del Pres de Egresos Detallado  CSPC.xlsx', tres: 'assets/pdfs/2017/Ley Disciplina Financiera 3/9 CLASIFICACION SERVICIO PROFESIONAL 3ER.xlsx',cuatro: 'assets/pdfs/2017/Ley Disciplina Financiera 3/9 CLASIFICACION SERVICIO PROFESIONAL 4TO.xlsx'},
  {diciplina: 'Estado de Situación Financiera Detallado Clasificación por Objeto del Gasto', uno: 'assets/pdfs/2017/Ley Disciplina Financiera 1/6 Estado de Situacion Financiera Detallado COG.xlsx', dos: 'assets/pdfs/2017/Ley Disciplina Financiera 2/6.A)-- Estado Ana del Ejer del Pres de Egresos Detallado  (COG).xlsx', tres: 'assets/pdfs/2017/Ley Disciplina Financiera 3/6 CLASIFICACION POR OBJETO DEL GASTO 3ER.xlsx',cuatro: 'assets/pdfs/2017/Ley Disciplina Financiera 3/6 CLASIFICACION POR OBJETO DEL GASTO 4TO.xlsx'}
];


@Component({
  selector: 'app-tabla-leydiciplinariafinanciera2017',
  templateUrl: './tabla-leydiciplinariafinanciera2017.component.html',
  styleUrls: ['./tabla-leydiciplinariafinanciera2017.component.css']
})
export class TablaLeydiciplinariafinanciera2017Component implements OnInit {
  displayedColumns: string[] = ['diciplina', 'uno', 'dos','tres','cuatro'];
  dataSourceDiciplina2017 = ELEMENT_DATA_DIS2017;
  constructor() { }

  ngOnInit() {
  }

}
