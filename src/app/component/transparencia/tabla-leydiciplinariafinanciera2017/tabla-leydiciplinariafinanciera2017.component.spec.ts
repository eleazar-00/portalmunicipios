import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaLeydiciplinariafinanciera2017Component } from './tabla-leydiciplinariafinanciera2017.component';

describe('TablaLeydiciplinariafinanciera2017Component', () => {
  let component: TablaLeydiciplinariafinanciera2017Component;
  let fixture: ComponentFixture<TablaLeydiciplinariafinanciera2017Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaLeydiciplinariafinanciera2017Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaLeydiciplinariafinanciera2017Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
